<?php
/**
 * Enqueue_Exercise Plugin
 *
 * @package Enqueue_Excercise
 * @version 1.0.0
 */

/**
 *
 * Plugin Name:       Enqueue Exercise Plugin
 * Plugin URI:        https://example.com/plugins/the-basics/
 * Description:       Esta es la descripcion.
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            German Fernandez
 * Author URI:        https://yithemes.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       yith-enqueue-scripts
 * Domain Path:       /languages
 */

wp_enqueue_style( 'style-color', plugin_dir_url( __FILE__ ) . 'assets/css/mystyle.css', array(), '1.0' );
