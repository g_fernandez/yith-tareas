<?php
/**
 * Shortcode_Exercise Plugin
 *
 * @package Shortcode_Excercise
 * @version 1.0.0
 */

/**
 *
 * Plugin Name:       Shortcode Exercise Plugin
 * Plugin URI:        https://example.com/plugins/the-basics/
 * Description:       Esta es la descripcion.
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            German Fernandez
 * Author URI:        https://yithemes.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       yith-shortcode
 * Domain Path:       /languages
 */

add_shortcode( 'yith_gf_simple_shortcode', 'yith_gf_simple_shortcode' );

/**
 * Yith_gf_simple_shortcode
 *
 * @param  mixed $atts Atts.
 * @param  mixed $content Post content.
 * @return $content
 */
function yith_gf_simple_shortcode( $atts, $content = null ) {
	$fields = shortcode_atts(
		array(
			'post-id' => 6,
		),
		$atts
	);

	$post_content = get_post( $fields['post-id'] );
	$content      = '<p> El contenido del post-' . $fields['post-id'] . ' es: ' . $post_content->post_content . '</p>';

	return $content;
}


add_shortcode( 'yith_gf_last_posts', 'yith_gf_last_posts' );

/**
 * Yith last posts
 *
 * @return $posts_titles
 */
function yith_gf_last_posts() {
	$posts_titles = '';
	$posts        = get_posts();
	foreach ( $posts as $key => $post ) {
		if ( $key <= 4 ) {
			$posts_titles .= '<p>Post: ' . $post->post_title . '</p><br>';
		}
	}

	return $posts_titles;

}


add_shortcode( 'yith_gf_user_shortcode', 'yith_gf_user_shortcode' );

/**
 * Yith user shortcode
 *
 * @param  mixed $atts Atts.
 * @return $user_data
 */
function yith_gf_user_shortcode( $atts ) {
	$fields = shortcode_atts(
		array(
			'user-id' => 3,
		),
		$atts
	);

	$user      = get_user_by( 'id', $fields['user-id'] );
	$user_data = '<p>Nombre:  ' . $user->user_firstname . '</p><br><p>Apellido: ' . $user->user_lastname . '</p><br><p>Nickname:  ' . $user->user_nicename . '</p><br>';

	return $user_data;
}








