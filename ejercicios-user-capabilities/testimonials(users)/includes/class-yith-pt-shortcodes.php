<?php
/**
 * YITH PT Plugin Testimonials.
 *
 * @package testimonials(users)
 */

if ( ! defined( 'YITH_PT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PT_Shortcodes' ) ) {

	/**
	 * YITH_PT_Shortcodes
	 */
	class YITH_PT_Shortcodes {

		/**
		 * Main Instance.
		 *
		 * @var YITH_PT_Shortcodes
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_PT_Shortcodes Main instance.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

			$shortcodes = array(
				'yith_pt_show_post_type'        => __CLASS__ . '::show_post_types',
				'yith_pt_add_last_testimonials' => __CLASS__ . '::yith_pt_add_last_testimonials',
			);
			foreach ( $shortcodes as $shortcode => $function ) {
				add_shortcode( $shortcode, $function );
			}

		}

		/**
		 * Show_post_types
		 *
		 * @param mixed $args Args.
		 * @return string '<div class="yith-pt-posts">' . ob_get_clean() . '</div>';
		 */
		public static function show_post_types( $args ) {

			wp_enqueue_style( 'yith-pt-frontend-shortcode-css' );
			wp_add_inline_style( 'yith-pt-frontend-shortcode-css', 'a{color: ' . get_option( 'yith_pt_shortcode_links_color' ) . ';}' );
			wp_add_inline_style( 'yith-pt-frontend-shortcode-css', '.yith-pt-post-container{border-radius: ' . get_option( 'yith_pt_shortcode_border' ) . 'px;}' );

			$args = shortcode_atts(
				array(
					'numberposts' => get_option( 'yith_pt_shortcode_number', 6 ),
					'show_image'  => get_option( 'yith_pt_shortcode_show_image', 'yes' ),
					'include'     => '',
					'hover'       => get_option( 'yith_pt_shortcode_hover_effect', '' ),
					'post_type'   => 'pt-testimonial',
					'taxonomy'    => '',
					'tax_query'   => '',
				),
				$args,
				'yith_action_products'
			);

			if ( '' !== $args['taxonomy'] ) {
				$args['tax_query'] = array(
					array(
						'taxonomy' => 'yith_tax_studient',
						'field'    => 'slug',
						'terms'    => $args['taxonomy'],
					),
				);
			}

			$posts = get_posts( $args );
			ob_start();

			foreach ( $posts as $post ) {
				$meta = get_post_meta( $post->ID );
				yith_pt_get_template(
					'/frontend/show-testimonials.php',
					array(
						'post'       => $post,
						'meta'       => $meta,
						'hover'      => $args['hover'],
						'show_image' => $args['show_image'],
					)
				);
			}

			return '<div class="yith-pt-posts">' . ob_get_clean() . '</div>';

		}


		/**
		 * Yith_pt_add_last_testimonials Shortcode
		 *
		 * @return string '<div class="yith-pt-last-testimonials">' . ob_get_clean() . '</div>'
		 */
		public static function yith_pt_add_last_testimonials() {
			do_action( 'new_to_publish', $array );

			$testimonials = get_transient( 'yith_pt_testimonial_transient' );

			if ( ! $testimonials ) {
				$args  = array(
					'numberposts' => 3,
					'post_type'   => 'pt-testimonial',
				);
				$posts = get_posts( $args );
				set_transient( 'yith_pt_testimonial_transient', $posts, 3 * HOUR_IN_SECONDS );
				$testimonials = get_transient( 'yith_pt_testimonial_transient' );
			}

			ob_start();

			?>
			<h2 class="widget-title subheading heading-size-3"><?php echo esc_html_e( 'Last 3 Testimonials', 'yith-plugin-testimonials' ); ?></h2>
			<nav role="navigation">
				<ul>
					<?php
					foreach ( $testimonials as $post ) {
						$titulo = $post->post_title;
						?>
						<li>
							<a href="<?php echo esc_url( get_permalink( $post->ID ) ); ?>"><?php echo esc_attr( $titulo ); ?></a>
						</li>
						<?php
					}
					?>
				</ul>
			</nav>
			<?php

			return '<div class="yith-pt-last-testimonials">' . ob_get_clean() . '</div>';
		}

	}

}
