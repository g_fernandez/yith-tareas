<?php
/**
 * YITH PT Plugin Testimonials.
 *
 * @package testimonials(users)
 */

if ( ! defined( 'YITH_PT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PT_Admin' ) ) {

	/**
	 * YITH_PT_Admin
	 */
	class YITH_PT_Admin {

		/**
		 * Main Instance
		 *
		 * @var $instance
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_PT_Admin Main instance.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'admin_enqueue_scripts', array( $this, 'yith_pt_enqueue_scripts' ) );

			// Meta Box.
			add_action( 'add_meta_boxes', array( $this, 'yith_pt_add_meta_boxes' ) );
			add_action( 'save_post', array( $this, 'yith_pt_save_meta_box' ) );

			// Posts Columns.
			add_filter( 'manage_pt-testimonial_posts_columns', array( $this, 'yith_pt_add_meta_columns' ) );
			add_action( 'manage_pt-testimonial_posts_custom_column', array( $this, 'yith_pt_display_meta_columns' ), 10, 2 );

			// Options Menu.
			add_action( 'admin_menu', array( $this, 'yith_pt_menu_page' ) );
			add_action( 'admin_init', array( $this, 'yith_pt_register_menu_page' ) );

			add_action( 'admin_init', array( $this, 'yith_pt_add_role' ) );
		}

		/**
		 * Yith_pt_enqueue_scripts
		 *
		 * @param  mixed $hook_suffix Hook Suffix.
		 * @return void
		 */
		public function yith_pt_enqueue_scripts( $hook_suffix ) {
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_script( 'yith-pt-admin-metaboxes-js', YITH_PT_DIR_ASSETS_JS_URL . '/metaboxes.js', array( 'wp-color-picker' ), YITH_PT_VERSION );
		}

		/**
		 * Add_meta_boxes
		 *
		 * @return void
		 */
		public function yith_pt_add_meta_boxes() {
			add_meta_box(
				'yith-pt-additional-information',
				__( 'Additional information', 'yith-plugin-testimonials' ),
				array(
					$this,
					'yith_pt_view_meta_boxes',
				),
				YITH_PT_Post_Types::$post_type,
			);
		}

		/**
		 * View_meta_boxes
		 *
		 * @param  mixed $post Post.
		 * @return void
		 */
		public function yith_pt_view_meta_boxes( $post ) {

			yith_pt_get_view( '/metaboxes/plugin-testimonials-info-metabox.php', array( 'post' => $post ) );
		}

		/**
		 * Save_meta_box
		 *
		 * @param  mixed $post_id Post ID.
		 * @return void
		 */
		public function yith_pt_save_meta_box( $post_id ) {

			if ( get_post_type( $post_id ) !== YITH_PT_Post_Types::$post_type ) {
				return;
			}

			if ( isset( $_POST['_yith_pt_role'] ) ) {
				update_post_meta( $post_id, '_yith_pt_role', $_POST['_yith_pt_role'] );
			}
			if ( isset( $_POST['_yith_pt_company'] ) ) {
				update_post_meta( $post_id, '_yith_pt_company', $_POST['_yith_pt_company'] );
			}
			if ( isset( $_POST['_yith_pt_url'] ) ) {
				update_post_meta( $post_id, '_yith_pt_url', $_POST['_yith_pt_url'] );
			}
			if ( isset( $_POST['_yith_pt_email'] ) ) {
				update_post_meta( $post_id, '_yith_pt_email', $_POST['_yith_pt_email'] );
			}
			if ( isset( $_POST['_yith_pt_rating'] ) ) {
				update_post_meta( $post_id, '_yith_pt_rating', $_POST['_yith_pt_rating'] );
			}
			if ( isset( $_POST['_yith_pt_vip'] ) ) {
				update_post_meta( $post_id, '_yith_pt_vip', $_POST['_yith_pt_vip'] );
			} else {
				update_post_meta( $post_id, '_yith_pt_vip', 'no' );
			}
			if ( isset( $_POST['_yith_pt_badge'] ) ) {
				update_post_meta( $post_id, '_yith_pt_badge', $_POST['_yith_pt_badge'] );
			} else {
				update_post_meta( $post_id, '_yith_pt_badge', 'no' );
			}
			if ( isset( $_POST['_yith_pt_badge_text'] ) ) {
				update_post_meta( $post_id, '_yith_pt_badge_text', $_POST['_yith_pt_badge_text'] );
			}
			if ( isset( $_POST['_yith_pt_badge_color'] ) ) {
				update_post_meta( $post_id, '_yith_pt_badge_color', $_POST['_yith_pt_badge_color'] );
			}

		}

		/**
		 * Yith_pt_add_meta_columns
		 *
		 * @param  mixed $post_columns New Columns.
		 * @return $post_columns
		 */
		public function yith_pt_add_meta_columns( $post_columns ) {

			$new_columns = apply_filters(
				'yith_pt_testimonials_custom_columns',
				array(
					'role'    => esc_html__( 'Role', 'yith-plugin-testimonials' ),
					'company' => esc_html__( 'Company', 'yith-plugin-testimonials' ),
					'email'   => esc_html__( 'Email', 'yith-plugin-testimonials' ),
					'stars'   => esc_html__( 'Stars', 'yith-plugin-testimonials' ),
					'vip'     => esc_html__( 'VIP', 'yith-plugin-testimonials' ),
				)
			);

			$post_columns = array_merge( $post_columns, $new_columns );

			return $post_columns;
		}

		/**
		 * Yith_pt_display_meta_columns
		 *
		 * @param  mixed $column_name Column Name.
		 * @param  mixed $post_id Post ID.
		 * @return void
		 */
		public function yith_pt_display_meta_columns( $column_name, $post_id ) {
			$meta_column = get_post_meta( $post_id );
			switch ( $column_name ) {
				case 'role':
					echo esc_attr( $meta_column['_yith_pt_role'][0] );
					break;
				case 'company':
					echo esc_attr( $meta_column['_yith_pt_company'][0] );
					break;
				case 'email':
					echo esc_attr( $meta_column['_yith_pt_email'][0] );
					break;
				case 'stars':
					echo esc_attr( $meta_column['_yith_pt_rating'][0] );
					break;
				case 'vip':
					echo esc_attr( $meta_column['_yith_pt_vip'][0] );
					break;
				default:
					break;
			}

		}

		/**
		 * Display New Options Menu
		 *
		 * @return void
		 */
		public function yith_pt_menu_page() {

			add_menu_page(
				esc_html__( 'Testimonials Options', 'yith-plugin-testimonials' ),
				esc_html__( 'Testimonials Options', 'yith-plugin-testimonials' ),
				'testimonials_options',
				'plugin_testimonials_options',
				array( $this, 'testimonials_custom_menu_page' ),
				'',
				40,
			);

		}

		/**
		 * Set menu options view
		 *
		 * @return void
		 */
		public function testimonials_custom_menu_page() {
			yith_pt_get_view( '/admin/plugin-options-panel.php', array() );
		}

		/**
		 * Yith_pt_register_menu_page
		 *
		 * @return void
		 */
		public function yith_pt_register_menu_page() {

			$page_name    = 'pt-options-page';
			$section_name = 'options_section';

			$setting_fields = array(
				array(
					'id'       => 'yith_pt_shortcode_number',
					'title'    => esc_html__( 'Number', 'yith-plugin-testimonials' ),
					'callback' => 'yith_pt_print_number_input',
				),
				array(
					'id'       => 'yith_pt_shortcode_show_image',
					'title'    => esc_html__( 'Show image', 'yith-plugin-testimonials' ),
					'callback' => 'yith_pt_show_image',
				),
				array(
					'id'       => 'yith_pt_shortcode_hover_effect',
					'title'    => esc_html__( 'Hover Effect', 'yith-plugin-testimonials' ),
					'callback' => 'yith_pt_hover_effect',
				),
				array(
					'id'       => 'yith_pt_shortcode_border',
					'title'    => esc_html__( 'Border Radius (Pixels)', 'yith-plugin-testimonials' ),
					'callback' => 'yith_pt_border_radius',
				),
				array(
					'id'       => 'yith_pt_shortcode_links_color',
					'title'    => esc_html__( 'Links Color', 'yith-plugin-testimonials' ),
					'callback' => 'yith_pt_links_color',
				),
			);

			add_settings_section(
				$section_name,
				esc_html__( 'Section', 'yith-plugin-testimonials' ),
				'',
				$page_name
			);

			foreach ( $setting_fields as $field ) {
				extract( $field );

				add_settings_field(
					$id,
					$title,
					array( $this, $callback ),
					$page_name,
					$section_name,
					array( 'label_for' => $id )
				);

				register_setting( $page_name, $id );
			}

		}

		/**
		 * Yith_pt_print_number_input
		 *
		 * @return void
		 */
		public function yith_pt_print_number_input() {
			$tst_number = intval( get_option( 'yith_pt_shortcode_number', 6 ) );
			?>
			<input
				type="number"
				id="yith_pt_shortcode_number"
				name="yith_pt_shortcode_number"
				value="<?php echo '' !== $tst_number ? esc_attr( $tst_number ) : 6; ?>">
			<?php
		}

		/**
		 * Yith_pt_show_image
		 *
		 * @return void
		 */
		public function yith_pt_show_image() {
			?>
			<input
				type="checkbox"
				name="yith_pt_shortcode_show_image"
				value='yes' id="yith_pt_shortcode_show_image"
				<?php checked( get_option( 'yith_pt_shortcode_show_image', '' ), 'yes' ); ?>
			>
			<?php
		}

		/**
		 * Yith_pt_hover_effect
		 *
		 * @return void
		 */
		public function yith_pt_hover_effect() {
			?>
			<select name="yith_pt_shortcode_hover_effect" id="yith_pt_shortcode_hover_effect">
				<option value="" <?php selected( get_option( 'yith_pt_shortcode_hover_effect', '' ), '' ); ?>>None</option>
				<option value="zoom" <?php selected( get_option( 'yith_pt_shortcode_hover_effect', '' ), 'zoom' ); ?>>Zoom</option>
				<option value="highlight" <?php selected( get_option( 'yith_pt_shortcode_hover_effect', '' ), 'highlight' ); ?>>Highlight</option>
			</select>
			<?php
		}

		/**
		 * Yith_pt_border_radius
		 *
		 * @return void
		 */
		public function yith_pt_border_radius() {
			$tst_number = floatval( get_option( 'yith_pt_shortcode_border', 7 ) );
			?>
			<input
				type="number"
				step=".01"
				id="yith_pt_shortcode_border"
				name="yith_pt_shortcode_border"
				value="<?php echo '' !== $tst_number ? esc_attr( $tst_number ) : 7; ?>">
			<?php
		}

		/**
		 * Yith_pt_border_radius
		 *
		 * @return void
		 */
		public function yith_pt_links_color() {
			?>
			<input
				type="text"
				value="<?php echo '' !== get_option( 'yith_pt_shortcode_links_color', '#0073aa' ) ? esc_attr( get_option( 'yith_pt_shortcode_links_color' ) ) : '#0073aa'; ?>"
				class="my-color-field"
				name="yith_pt_shortcode_links_color"
				id="yith_pt_shortcode_links_color"/>
			<?php
		}

		/**
		 * Adding TTMLS Manager Role & capabilities
		 */
		public function yith_pt_add_role() {
			wp_roles()->remove_role( 'TTMLS_Manager' );

			add_role(
				'TTMLS_Manager',
				'TTMLS Manager',
				array(
					'read_testimonials'             => true,
					'edit_testimonials'             => true,
					'edit_others_testimonials'      => true,
					'edit_published_testimonials'   => true,
					'delete_testimonials'           => true,
					'delete_published_testimonials' => true,
					'delete_others_testimonials'    => true,
					'delete_testimonial'            => true,
					'publish_testimonials'          => true,
					'upload_files'                  => true,
					'testimonials_options'          => true,
					'manage_studients'              => true,
					'edit_studients'                => true,
					'delete_studients'              => true,
					'assign_studients'              => true,
				)
			);

			$role_admin = get_role( 'administrator' );
			$role_admin->add_cap( 'read_testimonials' );
			$role_admin->add_cap( 'edit_testimonials' );
			$role_admin->add_cap( 'edit_others_testimonials' );
			$role_admin->add_cap( 'edit_published_testimonials' );
			$role_admin->add_cap( 'delete_testimonials' );
			$role_admin->add_cap( 'publish_testimonials' );
			$role_admin->add_cap( 'testimonials_options' );
			$role_admin->add_cap( 'manage_studients' );
			$role_admin->add_cap( 'edit_studients' );
			$role_admin->add_cap( 'delete_studients' );
			$role_admin->add_cap( 'assign_studients' );
		}

	}

}
