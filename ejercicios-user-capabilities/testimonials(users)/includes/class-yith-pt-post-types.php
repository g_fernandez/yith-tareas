<?php
/**
 * YITH PT Plugin Testimonials.
 *
 * @package testimonials(users)
 */

if ( ! defined( 'YITH_PT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PT_Post_Types' ) ) {

	/**
	 * YITH_PT_Post_Types
	 */
	class YITH_PT_Post_Types {

		/**
		 * Main Instance
		 *
		 * @var YITH_PT_Post_Types
		 */
		private static $instance;

		/**
		 * Post Type Name
		 *
		 * @var YITH_PT_Post_Types
		 */
		public static $post_type = 'pt-testimonial';

		/**
		 * Get_instance
		 *
		 * @return YITH_PT_Post_Types Main instance
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'yith_pt_my_custom_post_testimonial' ) );
			add_action( 'init', array( $this, 'yith_pt_register_taxonomy' ) );
		}

		/**
		 * My_custom_post_testimonial
		 *
		 * @return void
		 */
		public function yith_pt_my_custom_post_testimonial() {

			$args = array(
				'label'           => __( 'Testimonials', 'yith-plugin-testimonials' ),
				'labels'          => array(
					'singular_name'         => __( 'Testimonial', 'yith-plugin-testimonials' ),
					'add_new'               => __( 'Add New', 'yith-plugin-testimonials' ),
					'add_new_item'          => __( 'Add New Testimonial', 'yith-plugin-testimonials' ),
					'edit_item'             => __( 'Edit Testimonial', 'yith-plugin-testimonials' ),
					'new_item'              => __( 'New Testimonial', 'yith-plugin-testimonials' ),
					'view_item'             => __( 'View Testimonial', 'yith-plugin-testimonials' ),
					'view_items'            => __( 'View Testimonials', 'yith-plugin-testimonials' ),
					'search_items'          => __( 'Search Testimonials', 'yith-plugin-testimonials' ),
					'not_found'             => __( 'No Testimonials Found', 'yith-plugin-testimonials' ),
					'all_items'             => __( 'All Testimonials', 'yith-plugin-testimonials' ),
					'featured_image'        => __( 'Photo', 'yith-plugin-testimonials' ),
					'set_featured_image'    => __( 'Set Photo', 'yith-plugin-testimonials' ),
					'remove_featured_image' => __( 'Remove Photo', 'yith-plugin-testimonials' ),
				),
				'description'     => __( 'Testimonials post type', 'yith-plugin-testimonials' ),
				'public'          => false,
				'menu_icon'       => 'dashicons-universal-access',
				'show_in_menu'    => true,
				'show_ui'         => true,
				'rewrite'         => false,
				'supports'        => array( 'title', 'editor', 'thumbnail' ),
				'capability_type' => array( 'testimonial', 'testimonials' ),
				'map_meta_cap'    => true,
			);
			register_post_type( self::$post_type, $args );

		}

		/**
		 * Register Studient Taxonomy
		 *
		 * @return void
		 */
		public function yith_pt_register_taxonomy() {

			$labels = array(
				'name'              => _x( 'Studient Taxonomy', 'taxonomy general name', 'yith-plugin-testimonials' ),
				'singular_name'     => _x( 'Studient', 'taxonomy singular name', 'yith-plugin-testimonials' ),
				'search_items'      => __( 'Search Studient', 'yith-plugin-testimonials' ),
				'all_items'         => __( 'All Studient', 'yith-plugin-testimonials' ),
				'parent_item'       => __( 'Parent Studient', 'yith-plugin-testimonials' ),
				'parent_item_colon' => __( 'Parent Studient:', 'yith-plugin-testimonials' ),
				'edit_item'         => __( 'Edit Studient', 'yith-plugin-testimonials' ),
				'update_item'       => __( 'Update Studient', 'yith-plugin-testimonials' ),
				'add_new_item'      => __( 'Add New Studient', 'yith-plugin-testimonials' ),
				'new_item_name'     => __( 'New Studient Name', 'yith-plugin-testimonials' ),
				'menu_name'         => __( 'Studient', 'yith-plugin-testimonials' ),
			);

			$args = array(
				'hierarchical'      => false,
				'labels'            => $labels,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'yith_tax_studient' ),
				'capabilities' => array(
					'manage_terms' => 'manage_studients',
					'edit_terms' => 'edit_studients',
					'delete_terms' => 'delete_studients',
					'assign_terms' => 'assign_studients',
				),
			);

			register_taxonomy( 'yith_tax_studient', array( self::$post_type ), $args );

		}

	}
}
