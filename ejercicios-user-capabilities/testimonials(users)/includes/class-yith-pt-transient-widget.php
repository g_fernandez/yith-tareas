<?php
/**
 * Description.
 *
 * @package Testimonials(users)
 */

/**
 * Yith_Pt_Transient_Widget
 */
class Yith_Pt_Transient_Widget extends WP_Widget {

	/**
	 * Get Instance
	 *
	 * @return Yith_Pt_Transient_Widget
	 */
	public static function get_instance() {
		return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();

	}

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		$widget_ops = array(
			'classname'   => 'Yith_Pt_Transient_Widget',
			'description' => 'Three last Testimonials',
		);
		parent::__construct( 'yith_pt_transient_widget', 'Three last Testimonials', $widget_ops );
	}

	/**
	 * Output the widget content on the front-end
	 *
	 * @param  mixed $args Args.
	 * @param  mixed $instance Instance.
	 * @return void
	 */
	public function widget( $args, $instance ) {

		do_action( 'new_to_publish', $array );

		$testimonials = get_transient( 'yith_pt_testimonial_transient' );

		if ( ! $testimonials ) {
			$args  = array(
				'numberposts' => 3,
				'post_type'   => 'pt-testimonial',
			);
			$posts = get_posts( $args );
			set_transient( 'yith_pt_testimonial_transient', $posts, 3 * HOUR_IN_SECONDS );
			$testimonials = get_transient( 'yith_pt_testimonial_transient' );
		}

		echo $args['before_widget'];
		?>
		<h2 class="widget-title subheading heading-size-3"><?php echo esc_html_e( 'Last 3 Testimonials', 'yith-plugin-testimonials' ); ?></h2>
		<nav role="navigation">
			<ul>
				<?php
				foreach ( $testimonials as $post ) {
					$titulo = $post->post_title;
					?>
					<li>
						<a href="<?php echo esc_url( get_permalink( $post->ID ) ); ?>"><?php echo esc_attr( $titulo ); ?></a>
					</li>
					<?php
				}
				?>
			</ul>
		</nav>
		<?php
		echo $args['after_widget'];

	}

	/**
	 * Output the option form field in admin Widgets screen.
	 *
	 * @param  mixed $instance Instance.
	 * @return void
	 */
	public function form( $instance ) {
	}

	/**
	 * Update - Save options.
	 *
	 * @param  mixed $new_instance New Instance.
	 * @param  mixed $old_instance Old Instance.
	 * @return void
	 */
	public function update( $new_instance, $old_instance ) {
	}
}



