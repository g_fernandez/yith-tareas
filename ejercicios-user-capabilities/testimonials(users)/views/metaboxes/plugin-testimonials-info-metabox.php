<?php

?>

<div class="yith_pt_metabox">
	<div>
		<label for="testimonial_meta_role"><?php esc_attr_e( 'Role', 'yith-plugin-testimonials' ); ?></label>
		<input type="text" name="_yith_pt_role" id="testimonial_meta_role"
		value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_role', true ) ); ?>">
	</div>
	<div>
		<label for="testimonial_meta_company"><?php esc_attr_e( 'Company', 'yith-plugin-testimonials' ); ?></label>
		<input type="text" name="_yith_pt_company" id="testimonial_meta_company"
		value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_company', true ) ); ?>">
	</div>
	<div>
		<label for="testimonial_meta_url"><?php esc_attr_e( 'URL', 'yith-plugin-testimonials' ); ?></label>
		<input type="text" name="_yith_pt_url" id="testimonial_meta_url"
		value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_url', true ) ); ?>">
	</div>
	<div>
		<label for="testimonial_meta_email"><?php esc_attr_e( 'Email', 'yith-plugin-testimonials' ); ?></label>
		<input type="email" name="_yith_pt_email" id="testimonial_meta_email"
		value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_email', true ) ); ?>">
	</div>
	<div>
		<label for="testimonial_meta_rating"><?php esc_attr_e( 'Rating', 'yith-plugin-testimonials' ); ?></label>
		<input type="number" min="1" max="5" name="_yith_pt_rating" id="testimonial_meta_rating"
		value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_rating', true ) ); ?>">
	</div>
	<div>
		<label for="testimonial_meta_vip"><?php esc_attr_e( 'VIP', 'yith-plugin-testimonials' ); ?></label>
		<input type="checkbox" name="_yith_pt_vip" value='yes' id="testimonial_meta_vip"
			<?php checked( get_post_meta( $post->ID, '_yith_pt_vip', true ), 'yes' ); ?>>
	</div>
	<div>
		<label for="testimonial_meta_badge"><?php esc_attr_e( 'Enable badge', 'yith-plugin-testimonials' ); ?></label>
		<input type="checkbox" name="_yith_pt_badge" value='yes' id="testimonial_meta_badge"
			<?php checked( get_post_meta( $post->ID, '_yith_pt_badge', true ), 'yes' ); ?>>
	</div>
	<div id="badge">
		<label for="testimonial_meta_badge_text"><?php esc_attr_e( 'Badge Text', 'yith-plugin-testimonials' ); ?></label>
		<input type="text" name="_yith_pt_badge_text" id="testimonial_meta_badge_text"
			   value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_badge_text', true ) ); ?>">
	</div>
	<div>
		<label for="testimonial_meta_badge_color"><?php esc_attr_e( 'Badge Color', 'yith-plugin-testimonials' ); ?></label>
		<input type="text" value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_pt_badge_color', true ) ); ?>" class="my-color-field" name="_yith_pt_badge_color" id="testimonial_meta_badge_color"/>
	</div>
</div>
