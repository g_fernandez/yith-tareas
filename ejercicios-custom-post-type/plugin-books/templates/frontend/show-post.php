<?php
/**
 * YITH GF Plugin Books.
 *
 * @package plugin-books
 */

?>

	<div class="yith-gf-post-container">

		<div class="yith-gf-post-title">
			<?php echo esc_attr( $post->post_title ); ?>
		</div>

		<?php

		if ( 'yes' == $show_image ) {
			?>
			<div class="yith-gf-post-container-image">
				<?php
				echo get_the_post_thumbnail( $post->ID, 'thumbnail', array( 'class' => 'alignleft' ) );
				?>
			</div>
			<?php
		}

		?>

		<div class="yith-gf-post-content">
			<?php echo esc_attr( $post->post_content ); ?>
		</div>

		<div class="yith-gf-post-meta">
			<p> <?php echo esc_attr_e( 'ISBN: ' ); ?><?php echo esc_attr( $meta['_yith_gf_isbn'][0] ); ?></p>
			<p> <?php echo esc_attr_e( 'Price: ' ); ?><?php echo esc_attr( $meta['_yith_gf_price'][0] ); ?></p>
			<p> <?php echo esc_attr_e( 'Book Cover: ' ); ?><?php echo esc_attr( $meta['_yith_gf_cover'][0] ); ?></p>
			<p> <?php echo esc_attr_e( 'Language: ' ); ?><?php echo esc_attr( $meta['_yith_gf_lang'][0] ); ?></p>
		</div>

	</div>
