# Copyright (C) 2021 German Fernandez
# This file is distributed under the same license as the YITH Plugin Books plugin.
msgid ""
msgstr ""
"Project-Id-Version: YITH Plugin Books 1.0.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/plugin-books\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2021-03-25T12:59:18+00:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.5.0-alpha-3f4c34b\n"
"X-Domain: yith-plugin-books\n"

#. Plugin Name of the plugin
msgid "YITH Plugin Books"
msgstr ""

#. Description of the plugin
msgid "Books for YITH Plugins"
msgstr ""

#. Author of the plugin
msgid "German Fernandez"
msgstr ""

#. Author URI of the plugin
msgid "https://yithemes.com/"
msgstr ""

#: includes/class-yith-gf-admin.php:63
msgid "Additional information"
msgstr ""

#: includes/class-yith-gf-admin.php:120
#: views/metaboxes/plugin-books-info-metabox.php:7
msgid "ISBN"
msgstr ""

#: includes/class-yith-gf-admin.php:121
#: views/metaboxes/plugin-books-info-metabox.php:12
msgid "Price"
msgstr ""

#: includes/class-yith-gf-admin.php:122
#: views/metaboxes/plugin-books-info-metabox.php:17
msgid "Cover Type"
msgstr ""

#: includes/class-yith-gf-admin.php:123
#: views/metaboxes/plugin-books-info-metabox.php:22
msgid "Language"
msgstr ""

#: includes/class-yith-gf-admin.php:168
#: includes/class-yith-gf-admin.php:169
msgid "Books Options"
msgstr ""

#: includes/class-yith-gf-admin.php:197
msgid "Section"
msgstr ""

#: includes/class-yith-gf-admin.php:204
msgid "Number"
msgstr ""

#: includes/class-yith-gf-admin.php:223
msgid "Show image"
msgstr ""

#: includes/class-yith-gf-post-types.php:58
msgctxt "post type general name"
msgid "Books"
msgstr ""

#: includes/class-yith-gf-post-types.php:59
msgctxt "post type singular name"
msgid "Book"
msgstr ""

#: includes/class-yith-gf-post-types.php:60
msgctxt "book"
msgid "Add New"
msgstr ""

#: includes/class-yith-gf-post-types.php:61
msgid "Add New Book"
msgstr ""

#: includes/class-yith-gf-post-types.php:62
msgid "Edit Book"
msgstr ""

#: includes/class-yith-gf-post-types.php:63
msgid "New Book"
msgstr ""

#: includes/class-yith-gf-post-types.php:64
msgid "All Books"
msgstr ""

#: includes/class-yith-gf-post-types.php:65
msgid "View Book"
msgstr ""

#: includes/class-yith-gf-post-types.php:66
msgid "Search Books"
msgstr ""

#: includes/class-yith-gf-post-types.php:67
msgid "No books found"
msgstr ""

#: includes/class-yith-gf-post-types.php:68
msgid "No books found in the Trash"
msgstr ""

#: includes/class-yith-gf-post-types.php:73
msgid "Books"
msgstr ""

#: includes/class-yith-gf-post-types.php:74
msgid "Books post type"
msgstr ""

#: views/admin/plugin-options-panel.php:6
msgid "Settings"
msgstr ""
