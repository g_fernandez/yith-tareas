<?php
/**
 * YITH GF Plugin Books.
 *
 * @package plugin-books
 */

if ( ! defined( 'YITH_GF_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_GF_Post_Types' ) ) {

	/**
	 * YITH_GF_Post_Types
	 */
	class YITH_GF_Post_Types {

		/**
		 * Main Instance
		 *
		 * @var YITH_GF_Post_Typess
		 */
		private static $instance;

		/**
		 * Post Type Name
		 *
		 * @var YITH_GF_Post_Types
		 */
		public static $post_type = 'gf-books';

		/**
		 * Get_instance
		 *
		 * @return YITH_GF_Post_Types Main instance
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'init', array( $this, 'my_custom_post_book' ) );
		}

		/**
		 * My_custom_post_book
		 *
		 * @return void
		 */
		public function my_custom_post_book() {
			$labels = array(
				'name'               => _x( 'Books', 'post type general name', 'yith-plugin-books' ),
				'singular_name'      => _x( 'Book', 'post type singular name', 'yith-plugin-books' ),
				'add_new'            => _x( 'Add New', 'book', 'yith-plugin-books' ),
				'add_new_item'       => __( 'Add New Book', 'yith-plugin-books' ),
				'edit_item'          => __( 'Edit Book', 'yith-plugin-books' ),
				'new_item'           => __( 'New Book', 'yith-plugin-books' ),
				'all_items'          => __( 'All Books', 'yith-plugin-books' ),
				'view_item'          => __( 'View Book', 'yith-plugin-books' ),
				'search_items'       => __( 'Search Books', 'yith-plugin-books' ),
				'not_found'          => __( 'No books found', 'yith-plugin-books' ),
				'not_found_in_trash' => __( 'No books found in the Trash', 'yith-plugin-books' ),
				'menu_name'          => 'Books',
			);

			$args = array(
				'label'        => __( 'Books', 'yith-plugin-books' ),
				'description'  => __( 'Books post type', 'yith-plugin-books' ),
				'public'       => false,
				'menu_icon'    => 'dashicons-universal-access',
				'show_in_menu' => true,
				'show_ui'      => true,
				'rewrite'      => false,
				'supports'     => array( 'title', 'editor', 'thumbnail' ),
			);
			register_post_type( self::$post_type, $args );
		}

	}
}
