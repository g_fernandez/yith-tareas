<?php
/**
 * YITH GF Plugin Books.
 *
 * @package plugin-books
 */

if ( ! defined( 'YITH_GF_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_GF_Frontend' ) ) {

	/**
	 * YITH_GF_Frontend
	 */
	class YITH_GF_Frontend {

		/**
		 * YITH_GF_Frontend instance.
		 *
		 * @var YITH_GF_Frontend
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_GF_Frontend Main instance.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'wp_enqueue_scripts', array( $this, 'yith_gf_enqueue_styles' ) );
		}

		/**
		 * Enqueue_styles
		 *
		 * @return void
		 */
		public function yith_gf_enqueue_styles() {
			wp_register_style( 'yith-gf-frontend-shortcode-css', YITH_GF_DIR_ASSETS_CSS_URL . '/shortcode.css', array(), YITH_GF_VERSION );
		}

	}

}
