<?php
/**
 * YITH GF Plugin Books.
 *
 * @package plugin-books
 */

if ( ! defined( 'YITH_GF_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_GF_Shortcodes' ) ) {

	/**
	 * YITH_GF_Shortcodes
	 */
	class YITH_GF_Shortcodes {

		/**
		 * Main Instance.
		 *
		 * @var YITH_GF_Shortcodes
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_GF_Shortcodes Main instance.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

			$shortcodes = array(
				'yith_gf_show_post_type' => __CLASS__ . '::show_post_types',
			);
			foreach ( $shortcodes as $shortcode => $function ) {
				add_shortcode( $shortcode, $function );
			}

		}

		/**
		 * Show_post_types
		 *
		 * @param  mixed $args Args.
		 * @return '<div class="yith-gf-posts">' . ob_get_clean() . '</div>';
		 */
		public static function show_post_types( $args ) {

			wp_enqueue_style( 'yith-gf-frontend-shortcode-css' );

			$args = shortcode_atts(
				array(
					'numberpost' => get_option( 'yith_gf_shortcode_number', 10 ),
					'show_image' => get_option( 'yith_gf_shortcode_show_image', 'no' ),
					'post_type'  => 'gf-books',
				),
				$args,
				'yith_action_products',
			);

			$posts = get_posts( $args );

			ob_start();

			foreach ( $posts as $key => $post ) {
				if ( $key < $args['numberpost'] ) {
					$meta = get_post_meta( $post->ID );
					yith_gf_get_template(
						'/frontend/show-post.php',
						array(
							'post'       => $post,
							'meta'       => $meta,
							'show_image' => $args['show_image'],
						),
					);
				}
			}

			return '<div class="yith-gf-posts">' . ob_get_clean() . '</div>';

		}

	}

}
