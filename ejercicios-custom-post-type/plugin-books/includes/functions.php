<?php
/**
 * YITH GF Plugin Books.
 *
 * @package plugin-books
 */

if ( ! function_exists( 'yith_gf_get_template' ) ) {
	/**
	 * Include templates
	 *
	 * @param  mixed $file_name Name of file.
	 * @param  mixed $args Arguments.
	 * @return void
	 */
	function yith_gf_get_template( $file_name, $args = array() ) {
		extract( $args );
		$full_path = YITH_GF_DIR_TEMPLATES_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}

if ( ! function_exists( 'yith_gf_get_view' ) ) {
	/**
	 * Include views
	 *
	 * @param  mixed $file_name Name of file.
	 * @param  mixed $args Arguments.
	 * @return void
	 */
	function yith_gf_get_view( $file_name, $args = array() ) {
		extract( $args );
		$full_path = YITH_GF_DIR_VIEWS_PATH . $file_name;
		if ( file_exists( $full_path ) ) {
			include $full_path;
		}
	}
}
