<?php
/**
 * YITH GF Plugin Books.
 *
 * @package plugin-books
 */

if ( ! defined( 'YITH_GF_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_GF_Admin' ) ) {

	/**
	 * YITH_GF_Admin
	 */
	class YITH_GF_Admin {

		/**
		 * Main Instance
		 *
		 * @var $instance
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_GF_Admin Main instance.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

			// Meta Box.
			add_action( 'add_meta_boxes', array( $this, 'yith_gf_add_meta_boxes' ) );
			add_action( 'save_post', array( $this, 'yith_gf_save_meta_box' ) );

			// Posts Columns.
			add_filter( 'manage_gf-books_posts_columns', array( $this, 'yith_gf_add_post_type_columns' ) );
			add_action( 'manage_gf-books_posts_custom_column', array( $this, 'yith_gf_display_post_type_columns' ), 10, 2 );

			// Options Menu.
			add_action( 'admin_menu', array( $this, 'yith_gf_menu_page' ) );
			add_action( 'admin_init', array( $this, 'yith_gf_register_menu_page' ) );
		}

		/**
		 * Add_meta_boxes
		 *
		 * @return void
		 */
		public function yith_gf_add_meta_boxes() {
			add_meta_box(
				'yith-gf-additional-information',
				__( 'Additional information', 'yith-plugin-books' ),
				array(
					$this,
					'yith_gf_view_meta_boxes',
				),
				YITH_GF_Post_Types::$post_type,
			);
		}

		/**
		 * View_meta_boxes
		 *
		 * @param  mixed $post Post.
		 * @return void
		 */
		public function yith_gf_view_meta_boxes( $post ) {
			yith_gf_get_view( '/metaboxes/plugin-books-info-metabox.php', array( 'post' => $post ) );
		}

		/**
		 * Save_meta_box
		 *
		 * @param  mixed $post_id Post ID.
		 * @return void
		 */
		public function yith_gf_save_meta_box( $post_id ) {

			if ( get_post_type( $post_id ) !== YITH_GF_Post_Types::$post_type ) {
				return;
			}

			if ( isset( $_POST['_yith_gf_isbn'] ) ) {
				update_post_meta( $post_id, '_yith_gf_isbn', $_POST['_yith_gf_isbn'] );
			}
			if ( isset( $_POST['_yith_gf_price'] ) ) {
				update_post_meta( $post_id, '_yith_gf_price', $_POST['_yith_gf_price'] );
			}
			if ( isset( $_POST['_yith_gf_cover'] ) ) {
				update_post_meta( $post_id, '_yith_gf_cover', $_POST['_yith_gf_cover'] );
			}
			if ( isset( $_POST['_yith_gf_lang'] ) ) {
				update_post_meta( $post_id, '_yith_gf_lang', $_POST['_yith_gf_lang'] );
			}

		}

		/**
		 * Yith_gf_add_post_type_columns
		 *
		 * @param  mixed $post_columns New Columns.
		 * @return $post_columns
		 */
		public function yith_gf_add_post_type_columns( $post_columns ) {

			$new_columns = apply_filters(
				'yith_gf_books_custom_columns',
				array(
					'isbn'     => esc_html__( 'ISBN', 'yith-plugin-books' ),
					'price'    => esc_html__( 'Price', 'yith-plugin-books' ),
					'cover'    => esc_html__( 'Cover Type', 'yith-plugin-books' ),
					'language' => esc_html__( 'Language', 'yith-plugin-books' ),
				)
			);

			$post_columns = array_merge( $post_columns, $new_columns );

			return $post_columns;
		}

		/**
		 * Yith_gf_display_post_type_columns
		 *
		 * @param  mixed $column_name Column Name.
		 * @param  mixed $post_id Post ID.
		 * @return void
		 */
		public function yith_gf_display_post_type_columns( $column_name, $post_id ) {
			$meta_column = get_post_meta( $post_id );
			switch ( $column_name ) {
				case 'isbn':
					echo esc_attr( $meta_column['_yith_gf_isbn'][0] );
					break;
				case 'price':
					echo esc_attr( $meta_column['_yith_gf_price'][0] );
					break;
				case 'cover':
					echo esc_attr( $meta_column['_yith_gf_cover'][0] );
					break;
				case 'language':
					echo esc_attr( $meta_column['_yith_gf_lang'][0] );
					break;
				default:
					break;
			}

		}

		/**
		 * Display New Options Menu
		 *
		 * @return void
		 */
		public function yith_gf_menu_page() {

			add_menu_page(
				esc_html__( 'Books Options', 'yith-plugin-books' ),
				esc_html__( 'Books Options', 'yith-plugin-books' ),
				'manage_options',
				'plugin_books_options',
				array( $this, 'books_custom_menu_page' ),
				'',
				40,
			);

		}

		/**
		 * Set menu options view
		 *
		 * @return void
		 */
		public function books_custom_menu_page() {
			yith_gf_get_view( '/admin/plugin-options-panel.php', array() );
		}

		/**
		 * Yith_gf_register_menu_page
		 *
		 * @return void
		 */
		public function yith_gf_register_menu_page() {

			add_settings_section(
				'options_section',
				esc_html__( 'Section', 'yith-plugin-books' ),
				'',
				'gf-options-page',
			);

			add_settings_field(
				'yith_gf_shortcode_number',
				esc_html__( 'Number', 'yith-plugin-books' ),
				array(
					$this,
					'yith_gf_print_number_input',
				),
				'gf-options-page',
				'options_section',
				array(
					'label_for' => 'yith_gf_shortcode_number',
				)
			);

			register_setting(
				'gf-options-page',
				'yith_gf_shortcode_number',
			);

			add_settings_field(
				'yith_gf_shortcode_show_image',
				esc_html__( 'Show image', 'yith-plugin-books' ),
				array(
					$this,
					'yith_gf_print_show_image',
				),
				'gf-options-page',
				'options_section',
				array(
					'label_for' => 'yith_gf_shortcode_show_image',
				)
			);

			register_setting(
				'gf-options-page',
				'yith_gf_shortcode_show_image',
			);
		}

		/**
		 * Yith_gf_print_number_input
		 *
		 * @return void
		 */
		public function yith_gf_print_number_input() {
			$tst_number = intval( get_option( 'yith_gf_shortcode_number', 6 ) );
			?>
			<input type="number" id="yith_gf_shortcode_number" name="yith_gf_shortcode_number"
				value="<?php echo '' !== $tst_number ? $tst_number : 6; ?>">
			<?php
		}

		/**
		 * Yith_gf_print_show_image
		 *
		 * @return void
		 */
		public function yith_gf_print_show_image() {
			?>
			<input
				type="checkbox"
				name="yith_gf_shortcode_show_image"
				value='yes' id="yith_gf_shortcode_show_image"
				<?php checked( get_option( 'yith_gf_shortcode_show_image', '' ), 'yes' ); ?>
			>
			<?php
		}

	}

}
