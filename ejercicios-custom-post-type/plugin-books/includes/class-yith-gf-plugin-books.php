<?php
/**
 * YITH GF Plugin Books.
 *
 * @package plugin-books
 */

if ( ! defined( 'YITH_GF_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_GF_Plugin_Books' ) ) {
	/**
	 * YITH_GF_Plugin_Books
	 */
	class YITH_GF_Plugin_Books {

		/**
		 * Plugin Books instance.
		 *
		 * @var YITH_GF_Plugin_Books
		 */
		private static $instance;

		/**
		 * Admin class instance.
		 *
		 * @var YITH_GF_Admin
		 */
		public $admin = null;

		/**
		 * Frontend class instance.
		 *
		 * @var YITH_GF_Frontend
		 */
		public $frontend = null;

		/**
		 * Shortcodes class instance.
		 *
		 * @var YITH_GF_Shortcodes
		 */
		public $shortcodes = null;

		/**
		 * Get_instance
		 *
		 * @return YITH_GF_Plugin_Books Main instance.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();

		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

			$require = apply_filters(
				'yith_gf_require_class',
				array(
					'common'   => array(
						'includes/class-yith-gf-post-types.php',
						'includes/functions.php',
						'includes/class-yith-gf-shortcodes.php',
					),
					'admin'    => array(
						'includes/class-yith-gf-admin.php',
					),
					'frontend' => array(
						'includes/class-yith-gf-frontend.php',
					),
				)
			);

			$this->_require( $require );

			$this->init_classes();

			$this->init();
		}

		/**
		 * _require
		 *
		 * @param mixed $main_classes Main.
		 * @return void
		 */
		protected function _require( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' == $section || ( 'frontend' == $section && ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' == $section && is_admin() ) && file_exists( YITH_GF_DIR_PATH . $class ) ) {
						require_once( YITH_GF_DIR_PATH . $class );
					}
				}
			}
		}

		/**
		 * Init_classes
		 *
		 * @return void
		 */
		public function init_classes() {
			$this->shortcodes = YITH_GF_Shortcodes::get_instance();
			YITH_GF_Post_Types::get_instance();
		}

		/**
		 * Init
		 *
		 * @return void
		 */
		public function init() {
			if ( is_admin() ) {
				$this->admin = YITH_GF_Admin::get_instance();
			}

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = YITH_GF_Frontend::get_instance();
			}
		}

	}

}

if ( ! function_exists( 'yith_gf_plugin_books' ) ) {
	/**
	 * Yith_gf_plugin_books
	 *
	 * @return YITH_GF_Plugin_Books Instance.
	 */
	function yith_gf_plugin_books() {
		return YITH_GF_Plugin_Books::get_instance();
	}
}
