<?php

?>

<div>
	<div>
		<label for="book_meta_isbn"><?php esc_attr_e( 'ISBN', 'yith-plugin-books' ); ?></label>
		<input type="text" name="_yith_gf_isbn" id="book_meta_isbn"
		value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_gf_isbn', true ) ); ?>">
	</div>
	<div>
		<label for="book_meta_price"><?php esc_attr_e( 'Price', 'yith-plugin-books' ); ?></label>
		<input type="text" name="_yith_gf_price" id="book_meta_price"
		value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_gf_price', true ) ); ?>">
	</div>
	<div>
		<label for="book_meta_cover"><?php esc_attr_e( 'Cover Type', 'yith-plugin-books' ); ?></label>
		<input type="text" name="_yith_gf_cover" id="book_meta_cover"
		value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_gf_cover', true ) ); ?>">
	</div>
	<div>
		<label for="book_meta_lang"><?php esc_attr_e( 'Language', 'yith-plugin-books' ); ?></label>
		<input type="text" name="_yith_gf_lang" id="book_meta_lang"
		value="<?php echo esc_html( get_post_meta( $post->ID, '_yith_gf_lang', true ) ); ?>">
	</div>
</div>
