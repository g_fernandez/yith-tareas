<?php

?>

<div class="wrap">
	<h1><?php esc_html_e( 'Settings', 'yith-plugin-books' ); ?></h1>

	<form method="post" action="options.php">
		<?php
			settings_fields( 'gf-options-page' );
			do_settings_sections( 'gf-options-page' );
			submit_button();
		?>

	</form>
</div>
