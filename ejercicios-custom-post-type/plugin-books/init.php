<?php
/**
 * Plugin Name: YITH Plugin Books
 * Description: Books for YITH Plugins
 * Version: 1.0.0
 * Author: German Fernandez
 * Author URI: https://yithemes.com/
 * Text Domain: yith-plugin-books
 *
 * @package Plugin-Books
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! defined( 'YITH_GF_VERSION' ) ) {
	define( 'YITH_GF_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_GF_DIR_URL' ) ) {
	define( 'YITH_GF_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_GF_DIR_ASSETS_URL' ) ) {
	define( 'YITH_GF_DIR_ASSETS_URL', YITH_GF_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_GF_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_GF_DIR_ASSETS_CSS_URL', YITH_GF_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_GF_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_GF_DIR_ASSETS_JS_URL', YITH_GF_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_GF_DIR_PATH' ) ) {
	define( 'YITH_GF_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_GF_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_GF_DIR_INCLUDES_PATH', YITH_GF_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_GF_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_GF_DIR_TEMPLATES_PATH', YITH_GF_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_GF_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_GF_DIR_VIEWS_PATH', YITH_GF_DIR_PATH . '/views' );
}


if ( ! function_exists( 'yith_gf_init_classes' ) ) {

	/**
	 * Yith_gf_init_classes
	 *
	 * @return void
	 */
	function yith_gf_init_classes() {

		load_plugin_textdomain( 'yith-plugin-books', false, basename( dirname( __FILE__ ) ) . '/languages' );
		require_once YITH_GF_DIR_INCLUDES_PATH . '/class-yith-gf-plugin-books.php';
		if ( class_exists( 'YITH_GF_Plugin_Books' ) ) {
			yith_gf_plugin_books();
		}
	}
}

add_action( 'plugins_loaded', 'yith_gf_init_classes' );
