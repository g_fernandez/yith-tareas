<?php
?>
<form id="yith_pl_form">
	<input type="hidden" name="yith_pl_name" id="yith_pl_name" value="<?php echo esc_attr( $user->user_firstname ); ?>">
	<input type="hidden" name="yith_pl_surname" id="yith_pl_surname" value="<?php echo esc_attr( $user->user_lastname ); ?>">
	<input type="hidden" name="yith_pl_email" id="yith_pl_email" value="<?php echo esc_attr( $user->user_email ); ?>">

	<label id="checkbox_label" for="yith_pl_confirm"><?php echo esc_html__( 'Yes, I want to participate in the lottery', 'yith-plugin-lottery' ); ?></label>
	<input type="checkbox" name="yith_pl_confirm" id="yith_pl_confirm">

	<input type="submit" name="yith_pl_submit" id="yith_pl_submit" value="<?php echo esc_html__( 'Subscribe me', 'yith-plugin-lottery' ); ?>">
</form>
