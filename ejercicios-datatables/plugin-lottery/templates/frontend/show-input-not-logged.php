<?php
?>
<form id="yith_pl_form">
	<label for="yith_pl_name"><?php echo esc_html__( 'Name', 'yith-plugin-lottery' ); ?></label>
	<input type="text" name="yith_pl_name" id="yith_pl_name" required>

	<label for="yith_pl_surname"><?php echo esc_html__( 'Surname', 'yith-plugin-lottery' ); ?></label>
	<input type="text" name="yith_pl_surname" id="yith_pl_surname" required>

	<label for="yith_pl_email"><?php echo esc_html__( 'Email', 'yith-plugin-lottery' ); ?></label>
	<input type="email" name="yith_pl_email" id="yith_pl_email" required>

	<label id="checkbox_label" for="yith_pl_confirm"><?php echo esc_html__( 'Yes, I want to participate in the lottery', 'yith-plugin-lottery' ); ?></label>
	<input type="checkbox" name="yith_pl_confirm" id="yith_pl_confirm">

	<input type="submit" name="yith_pl_submit" id="yith_pl_submit" value="<?php echo esc_html__( 'Subscribe me', 'yith-plugin-lottery' ); ?>">
</form>
