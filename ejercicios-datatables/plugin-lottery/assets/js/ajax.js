jQuery( document ).ready( function() {

  jQuery("#yith_pl_submit").click( function(e) {
    e.preventDefault();

    name = jQuery("#yith_pl_name").val();
    surname = jQuery("#yith_pl_surname").val();
    email = jQuery("#yith_pl_email").val();
    ischecked = jQuery("#yith_pl_confirm").prop('checked');

    if ( ischecked ) {
      checked = "on";
    } else {
      checked = "off";
    }

    jQuery.ajax({
      type: "POST",
      dataType: "json",
      url: myAjax.ajaxurl,
      data: {
        action: "submit_button",
        name: name,
        surname: surname,
        email: email,
        checked: checked,
        exist: "no",
        no_data: "no",
      },
      success: function (response) {
        if ( 'no' === response.exist && 'no' === response.no_data ) {
          alert("Congratulations, you have entered the draw. Good luck!");
        } else {
          if ( 'yes' === response.exist ) {
            alert("This user has already been registered in the draw previously");
          } else {
            if ( 'off' === response.checked ) {
              alert("Please confirm if you want to participate in the lottery");
            } else {
              alert("Missing data");
            }
          }

        }

      },
      error: function (response) {
        alert("Error");
      }
    })

  })

})
