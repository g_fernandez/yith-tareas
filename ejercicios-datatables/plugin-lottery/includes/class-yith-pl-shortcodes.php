<?php
/**
 * YITH PL Plugin Lottery.
 *
 * @package plugin-lottery
 */

if ( ! defined( 'YITH_PL_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PL_Shortcodes' ) ) {

	/**
	 * YITH_PL_Shortcodes
	 */
	class YITH_PL_Shortcodes {

		/**
		 * Main Instance.
		 *
		 * @var YITH_PL_Shortcodes
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_PL_Shortcodes Main instance.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

			$shortcodes = array(
				'yith_pl_shortcode_input' => __CLASS__ . '::shortcode_input',
			);
			foreach ( $shortcodes as $shortcode => $function ) {
				add_shortcode( $shortcode, $function );
			}

		}

		/**
		 * Shortcode for lottery inputs
		 *
		 * @return string
		 */
		public static function shortcode_input() {

			wp_enqueue_style( 'yith-pl-frontend-shortcode-css' );

			ob_start();

			if ( is_user_logged_in() ) {
				yith_pl_get_template( '/frontend/show-input-logged.php', array( 'user' => wp_get_current_user() ) );
			} else {
				yith_pl_get_template( '/frontend/show-input-not-logged.php' );
			}


			return '<div class="yith-pl-lottery">' . ob_get_clean() . '</div>';

		}

	}

}
