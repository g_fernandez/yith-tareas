<?php
/**
 * YITH PL Plugin Lottery.
 *
 * @package plugin-lottery
 */

if ( ! defined( 'YITH_PL_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PL_Frontend' ) ) {

	/**
	 * YITH_PL_Frontend
	 */
	class YITH_PL_Frontend {

		/**
		 * YITH_PL_Frontend instance.
		 *
		 * @var YITH_PL_Frontend
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_PL_Frontend Main instance.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'wp_enqueue_scripts', array( $this, 'yith_pl_enqueue_styles' ) );
		}

		/**
		 * Enqueue_styles
		 *
		 * @return void
		 */
		public function yith_pl_enqueue_styles() {
			wp_register_style( 'yith-pl-frontend-shortcode-css', YITH_PL_DIR_ASSETS_CSS_URL . '/shortcode.css', array(), YITH_PL_VERSION );
		}

	}

}
