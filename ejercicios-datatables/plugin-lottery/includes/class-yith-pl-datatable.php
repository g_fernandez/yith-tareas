<?php
/**
 * YITH PL Plugin Lottery.
 *
 * @package plugin-lottery
 */

if ( ! defined( 'YITH_PL_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PL_Datatable' ) ) {
	/**
	 * YITH_PL_Plugin_Lottery
	 */
	class YITH_PL_Datatable {

		/**
		 * Plugin Testimonials instance.
		 *
		 * @var YITH_PL_Datatable
		 */
		private static $instance;

		/**
		 * DB version
		 *
		 * @var string
		 */
		public static $version = '1.0.0';

		/**
		 * DB Name
		 *
		 * @var string
		 */
		public static $auction_table = 'yith_raffle';

		/**
		 * Get_instance
		 *
		 * @return YITH_PL_Datatable Main instance.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();

		}

		/**
		 * Constructor
		 */
		private function __construct() {
			self::create_db_table();
		}

		/**
		 * Create table for Notes
		 *
		 * @param bool $force
		 */
		public static function create_db_table() {
			global $wpdb;

			$current_version = get_option( 'yith_pl_db_version' );
			if ( $current_version !== self::$version ) {

				$wpdb->hide_errors();

				$table_name      = $wpdb->prefix . self::$auction_table;
				$charset_collate = $wpdb->get_charset_collate();

				$sql = "CREATE TABLE $table_name (
						id bigint(20) NOT NULL AUTO_INCREMENT,
						nombre varchar(255) NOT NULL,
						apellido varchar(255) NOT NULL,
						email varchar(255) NOT NULL,
						PRIMARY KEY (id)
						) $charset_collate;";

				if ( ! function_exists( 'dbDelta' ) ) {
					require_once ABSPATH . 'wp-admin/includes/upgrade.php';
				}
				dbDelta( $sql );
				update_option( 'yith_pl_db_version', self::$version );

			}
		}

	}
}
