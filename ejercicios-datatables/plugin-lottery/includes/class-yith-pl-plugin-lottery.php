<?php
/**
 * YITH PL Plugin Lottery.
 *
 * @package plugin-lottery
 */

if ( ! defined( 'YITH_PL_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PL_Plugin_Lottery' ) ) {
	/**
	 * YITH_PL_Plugin_Lottery
	 */
	class YITH_PL_Plugin_Lottery {

		/**
		 * Plugin Lottery instance.
		 *
		 * @var YITH_PL_Plugin_Lottery
		 */
		private static $instance;

		/**
		 * Frontend class instance.
		 *
		 * @var YITH_PL_Frontend
		 */
		public $frontend = null;

		/**
		 * Shortcodes class instance.
		 *
		 * @var YITH_PL_Shortcodes
		 */
		public $shortcodes = null;

		/**
		 * Datatable class instance.
		 *
		 * @var YITH_PL_Datatable
		 */
		public $datatable = null;

//		/**
//		 * Ajax class instance.
//		 *
//		 * @var YITH_PL_Ajax
//		 */
//		public $ajax = null;


		/**
		 * Get_instance
		 *
		 * @return YITH_PL_Plugin_Lottery Main instance.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();

		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

			$require = apply_filters(
				'yith_pl_require_class',
				array(
					'common'   => array(
						'includes/class-yith-pl-datatable.php',
						'includes/class-yith-pl-shortcodes.php',
						'includes/functions.php',
						'includes/class-yith-pl-ajax.php',
					),
					'frontend' => array(
						'includes/class-yith-pl-frontend.php',
					),
				)
			);

			$this->_require( $require );

			$this->init_classes();

			$this->init();

		}

		/**
		 * _require
		 *
		 * @param mixed $main_classes Main.
		 * @return void
		 */
		protected function _require( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' === $section || ( 'frontend' === $section && ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) && file_exists( YITH_PL_DIR_PATH . $class ) ) {
						require_once YITH_PL_DIR_PATH . $class;
					}
				}
			}
		}

		/**
		 * Init_classes
		 *
		 * @return void
		 */
		public function init_classes() {
			$this->datatable  = YITH_PL_Datatable::get_instance();
			$this->shortcodes = YITH_PL_Shortcodes::get_instance();
			$this->ajax       = YITH_PL_Ajax::get_instance();
		}

		/**
		 * Init
		 *
		 * @return void
		 */
		public function init() {

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = YITH_PL_Frontend::get_instance();
			}
		}

	}

}

if ( ! function_exists( 'yith_pl_plugin_lottery' ) ) {
	/**
	 * Yith_pl_plugin_lottery
	 *
	 * @return YITH_PL_Plugin_Lottery Instance.
	 */
	function yith_pl_plugin_lottery() {
		return YITH_PL_Plugin_Lottery::get_instance();
	}
}
