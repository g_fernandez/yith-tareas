<?php
/**
 * YITH PL Plugin Lottery.
 *
 * @package plugin-lottery
 */

if ( ! defined( 'YITH_PL_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PL_Ajax' ) ) {

	/**
	 * YITH_PL_Admin
	 */
	class YITH_PL_Ajax {

		/**
		 * Main Instance
		 *
		 * @var $instance
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_PL_Ajax Main instance.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'wp_enqueue_scripts', array( $this, 'yith_pl_enqueue_script_ajax' ) );
			add_action( 'wp_ajax_submit_button', array( $this, 'submit_button' ) );
			add_action( 'wp_ajax_nopriv_submit_button', array( $this, 'submit_button' ) );
		}

		/**
		 * Ajax Scripts
		 */
		public function yith_pl_enqueue_script_ajax() {
			wp_register_script( 'lottery_script', YITH_PL_DIR_ASSETS_JS_URL . '/ajax.js', array( 'jquery' ), YITH_PL_VERSION, false );
			wp_localize_script( 'lottery_script', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
			wp_enqueue_script( 'jquery' );
			wp_enqueue_script( 'lottery_script' );

		}

		/**
		 * Votes Button Up Click
		 */
		public function submit_button() {
			global $wpdb;

			if ( '' !== $_POST['name'] && '' !== $_POST['surname'] && '' !== $_POST['email'] && 'on' === $_POST['checked'] ) {
				$name    = $_POST['name'];
				$surname = $_POST['surname'];
				$email   = $_POST['email'];
				$checked = $_POST['checked'];
			} else {
				$output = array(
					'no_data' => 'yes',
					'checked' => $_POST['checked']
				);
				echo wp_json_encode( $output );
				die();
			}

			$registro = $wpdb->get_results( $wpdb->prepare("SELECT id FROM wp_yith_raffle W WHERE (W.nombre=%s AND W.apellido=%s AND W.email=%s)", $name, $surname, $email ) );

			if ( empty($registro) ) {

				$table  = $wpdb->prefix . 'yith_raffle';
				$data   = array(
					'nombre'   => $name,
					'apellido' => $surname,
					'email'    => $email,
				);

				$format = array( '%s', '%s', '%s' );

				$wpdb->insert( $table, $data, $format );
				$output = array(
					'exist'   => 'no',
					'no_data' => 'no',
				);
			} else {
				$output = array( 'exist' => 'yes' );
			}

			echo wp_json_encode( $output );
			die();

		}

	}
}
