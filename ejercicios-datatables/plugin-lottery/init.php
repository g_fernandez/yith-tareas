<?php
/**
 * Plugin Name: YITH Plugin Lottery
 * Descriplion: Lottery for YITH Plugins
 * Version: 1.0.0
 * Author: German Fernandez
 * Author URI: https://yithemes.com/
 * Text Domain: yith-plugin-lottery
 *
 * @package plugin-sorteo
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! defined( 'YITH_PL_VERSION' ) ) {
	define( 'YITH_PL_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_PL_DIR_URL' ) ) {
	define( 'YITH_PL_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_PL_DIR_ASSETS_URL' ) ) {
	define( 'YITH_PL_DIR_ASSETS_URL', YITH_PL_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_PL_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_PL_DIR_ASSETS_CSS_URL', YITH_PL_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_PL_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_PL_DIR_ASSETS_JS_URL', YITH_PL_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_PL_DIR_PATH' ) ) {
	define( 'YITH_PL_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_PL_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_PL_DIR_INCLUDES_PATH', YITH_PL_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_PL_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_PL_DIR_TEMPLATES_PATH', YITH_PL_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_PL_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_PL_DIR_VIEWS_PATH', YITH_PL_DIR_PATH . '/views' );
}


if ( ! function_exists( 'yith_pl_init_classes' ) ) {

	/**
	 * Yith_pl_init_classes
	 *
	 * @return void
	 */
	function yith_pl_init_classes() {

		$i = load_plugin_textdomain( 'yith-plugin-lottery', false, basename( dirname( __FILE__ ) ) . '/languages' );
		require_once YITH_PL_DIR_INCLUDES_PATH . '/class-yith-pl-plugin-lottery.php';
		if ( class_exists( 'YITH_PL_Plugin_Lottery' ) ) {
			yith_pl_plugin_lottery();
		}
	}
}

add_action( 'plugins_loaded', 'yith_pl_init_classes' );
