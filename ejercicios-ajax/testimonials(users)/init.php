<?php
/**
 * Plugin Name: YITH Plugin Testimonials (Users)
 * Description: Testimonials (Users) for YITH Plugins
 * Version: 1.3.0
 * Author: German Fernandez
 * Author URI: https://yithemes.com/
 * Text Domain: yith-plugin-testimonials
 *
 * @package Testimonials(users)
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! defined( 'YITH_PT_VERSION' ) ) {
	define( 'YITH_PT_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_PT_DIR_URL' ) ) {
	define( 'YITH_PT_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_PT_DIR_ASSETS_URL' ) ) {
	define( 'YITH_PT_DIR_ASSETS_URL', YITH_PT_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_PT_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_PT_DIR_ASSETS_CSS_URL', YITH_PT_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_PT_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_PT_DIR_ASSETS_JS_URL', YITH_PT_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_PT_DIR_PATH' ) ) {
	define( 'YITH_PT_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_PT_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_PT_DIR_INCLUDES_PATH', YITH_PT_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_PT_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_PT_DIR_TEMPLATES_PATH', YITH_PT_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_PT_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_PT_DIR_VIEWS_PATH', YITH_PT_DIR_PATH . '/views' );
}


if ( ! function_exists( 'yith_pt_init_classes' ) ) {

	/**
	 * Yith_pt_init_classes
	 *
	 * @return void
	 */
	function yith_pt_init_classes() {

		$i = load_plugin_textdomain( 'yith-plugin-testimonials', false, basename( dirname( __FILE__ ) ) . '/languages' );
		require_once YITH_PT_DIR_INCLUDES_PATH . '/class-yith-pt-plugin-testimonials.php';
		if ( class_exists( 'YITH_PT_Plugin_Testimonials' ) ) {
			yith_pt_plugin_testimonials();
		}
	}
}

add_action( 'plugins_loaded', 'yith_pt_init_classes' );

add_action(
	'widgets_init',
	function() {
		register_widget( 'yith_pt_transient_widget' );
	}
);


/**
 * Delete transient when new post is added/deleted.
 *
 * @return void
 */
function yith_pt_new_to_publish() {
	delete_transient( 'yith_pt_testimonial_transient' );
};

add_action( 'new_to_publish', 'yith_pt_new_to_publish', 10, 1 );
