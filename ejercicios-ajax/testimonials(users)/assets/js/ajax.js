jQuery( document ).ready( function() {

  jQuery(".button-up").click( function(e) {
    e.preventDefault();
    post_id = jQuery(this).attr("data-post_id");

    jQuery.ajax({
      type: "POST",
      dataType: "json",
      url: myAjax.ajaxurl,
      data: {
        action: "votes_button_up_click",
        post_id: post_id,
        votes: 0,

      },
      success: function (response) {
          jQuery("#vote_up_"+post_id).html(response.votes);
      }
    })

  })

  jQuery(".button-down").click( function(e) {
    e.preventDefault();
    post_id = jQuery(this).attr("data-post_id");
    jQuery.ajax({
      type: "POST",
      dataType: "json",
      url: myAjax.ajaxurl,
      data: {
        action: "votes_button_down_click",
        post_id: post_id,
        votes: 0,

      },
      success: function (response) {
        jQuery("#vote_down_"+post_id).html(response.votes);
      },
      error: function (response) {
        console.log("error: "+ response);
      }
    })

  })
})

