<?php
/**
 * YITH PT Plugin Testimonials.
 *
 * @package testimonials
 */
?>
	<div class="yith-pt-post-container <?php
	if ( isset( $meta ) ) {
		if ( 'yes' == $meta['_yith_pt_vip'][0] ) {
			?>vip<?php
		}
	}
	?>">
		<?php
		if ('yes' == $meta['_yith_pt_badge'][0]) {
		?>
			<span class="yith-pt-meta-badge" style="background: #6645b2;">
				<?php echo esc_attr( $meta['_yith_pt_badge_text'][0] ); ?>
			</span>
		<?php
		}
		?>

		<div class="header">

			<div class="left">
				<?php
				if ( isset( $show_image ) ) {
					if ( 'yes' == $show_image ) {
						?>
						<div class="yith-pt-post-container-image">
							<?php
							echo get_the_post_thumbnail( $post->ID, 'thumbnail', array( 'class' => 'alignleft' ) );
							?>
						</div>
						<?php
					}
				}
				?>
			</div>

			<div class="right">
				<div class="yith-pt-post-title">
					<?php echo esc_attr( $post->post_title ); ?>
				</div>
				<div class="yith-pt-post-company">
					<?php echo esc_attr( $meta['_yith_pt_role'][0] ) . ' at '; ?>
					<a href="<?php echo esc_attr( $meta['_yith_pt_url'][0] ); ?>"><?php echo esc_attr( $meta['_yith_pt_company'][0] ); ?></a>
				</div>
				<div class="yith-pt-post-email">
					<a href="mailto:<?php echo esc_attr( $meta['_yith_pt_email'][0] ); ?>" ><?php echo esc_attr( $meta['_yith_pt_email'][0] ); ?></a>
				</div>
			</div>

		</div>

		<div class="yith-pt-rating-stars">
			<?php
			for ( $i = 1; $i <= 5; $i++ ) {
				if ( $i <= $meta['_yith_pt_rating'][0] ) {
					?>
					<span class="fa fa-star checked"></span>
					<?php
				} else {
					?>
					<span class="fa fa-star"></span>
					<?php
				}
			}
			?>
		</div>

		<div class="yith-pt-post-content">
			<?php echo esc_attr( $post->post_content ); ?>
		</div>

		<?php
		if ( is_user_logged_in() ) {
			if ( isset( $enable_vote ) ) {
				if ( 'yes' == $enable_vote ) {
					?>
					<div class="yith-pt-post-votes">
						<div class="yith-pt-votes-up">
							<button
								class="button-up"
								data-post_id="<?php echo esc_attr( $post->ID ); ?>">
								<i class="fa fa-caret-up" aria-hidden="true"></i>
							</button>
							<p id="vote_up_<?php echo esc_attr( $post->ID ); ?>">
								<?php
								if ( isset( $meta['yith_pt_votes_up'][0] ) ) {
									echo esc_attr( $meta['yith_pt_votes_up'][0] );
								} else {
									?>
									0
									<?php
								}
								?>
							</p>

						</div>
						<div class="yith-pt-votes-down">
							<button
								class="button-down"
								data-post_id="<?php echo esc_attr( $post->ID ); ?>">
								<i class="fa fa-caret-down" aria-hidden="true"></i>
							</button>
							<p id="vote_down_<?php echo esc_attr( $post->ID ); ?>">
								<?php
								if ( isset( $meta['yith_pt_votes_down'][0] ) ) {
									echo esc_attr( $meta['yith_pt_votes_down'][0] );
								} else {
									?>
									0
									<?php
								}
								?>
							</p>
						</div>
					</div>
					<?php
				}
			}
		}
		?>

	</div>

	<?php
	if ( isset( $hover ) ) {
		if ( 'zoom' === $hover ) {
			?>
			<style>
				.yith-pt-post-container:hover {
					transform: scale(1.5);
				}
			</style>
			<?php
		} elseif ( 'highlight' === $hover ) {
			?>
			<style>
				.yith-pt-post-container:hover {
					border-color: lightblue;
				}
			</style>
			<?php
		}
	}
	?>
