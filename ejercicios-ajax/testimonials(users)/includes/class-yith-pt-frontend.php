<?php
/**
 * YITH PT Plugin Testimonials.
 *
 * @package testimonials(users)
 */

if ( ! defined( 'YITH_PT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PT_Frontend' ) ) {

	/**
	 * YITH_PT_Frontend
	 */
	class YITH_PT_Frontend {

		/**
		 * YITH_PT_Frontend instance.
		 *
		 * @var YITH_PT_Frontend
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_PT_Frontend Main instance.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'wp_enqueue_scripts', array( $this, 'yith_pt_enqueue_styles' ) );
		}

		/**
		 * Enqueue_styles
		 *
		 * @return void
		 */
		public function yith_pt_enqueue_styles() {
			wp_register_style( 'yith-pt-frontend-shortcode-css', YITH_PT_DIR_ASSETS_CSS_URL . '/shortcode.css', array(), YITH_PT_VERSION );
		}

	}

}
