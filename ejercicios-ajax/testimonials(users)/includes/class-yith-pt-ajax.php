<?php
/**
 * YITH PT Plugin Testimonials.
 *
 * @package testimonials(users)
 */

if ( ! defined( 'YITH_PT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PT_Ajax' ) ) {

	/**
	 * YITH_PT_Admin
	 */
	class YITH_PT_Ajax {

		/**
		 * Main Instance
		 *
		 * @var $instance
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_PT_Ajax Main instance.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'wp_enqueue_scripts', array( $this, 'yith_pt_enqueue_script_ajax' ) );
			add_action( 'wp_ajax_votes_button_up_click', array( $this, 'votes_button_up_click' ) );
			add_action( 'wp_ajax_votes_button_down_click', array( $this, 'votes_button_down_click' ) );
			//add_action( 'wp_ajax_nopriv_votes_button_click', array( $this, 'votes_button_click' ) );
		}

		/**
		 * Ajax Scripts
		 */
		public function yith_pt_enqueue_script_ajax() {
			wp_register_script( 'my_voter_script', YITH_PT_DIR_ASSETS_JS_URL . '/ajax.js', array( 'jquery' ), YITH_PT_VERSION, false );
			wp_localize_script( 'my_voter_script', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
			wp_enqueue_script( 'jquery' );
			wp_enqueue_script( 'my_voter_script' );

		}

		/**
		 * Votes Button Up Click
		 */
		public function votes_button_up_click() {
			$i = false;
			$current_user = wp_get_current_user();
			$user_votes = get_post_meta( $_POST['post_id'],'yith_pt_user_vote', true );
			$votes     = get_post_meta( $_POST['post_id'], 'yith_pt_votes_up', true );
			$votes     = ( '' === $votes ) ? 0 : $votes;
			if ( ! empty( $user_votes ) ) {
				foreach ( $user_votes as $user_vote ){
					if ( $current_user->ID === $user_vote ) {
						$i = true;
					}
				}
				if ( ! $i ) {
					++$votes;
					update_post_meta( $_POST['post_id'], 'yith_pt_votes_up', $votes );
					array_push( $user_votes, $current_user->ID );
					update_post_meta( $_POST['post_id'], 'yith_pt_user_vote', $user_votes );
				}
				$output = array( 'votes' => $votes );
			} else {
				++$votes;
				update_post_meta( $_POST['post_id'], 'yith_pt_votes_up', $votes );
				$user_votes = array($current_user->ID);
				update_post_meta( $_POST['post_id'], 'yith_pt_user_vote', $user_votes );
				$output = array( 'votes' => $votes );
			}

			echo wp_json_encode( $output );
			die();

		}

		/**
		 * Votes Button Down Click
		 */
		public function votes_button_down_click() {
			$i = false;
			$current_user = wp_get_current_user();
			$user_votes = get_post_meta( $_POST['post_id'],'yith_pt_user_vote', true );
			$votes     = get_post_meta( $_POST['post_id'], 'yith_pt_votes_down', true );
			$votes     = ( '' === $votes ) ? 0 : $votes;
			if ( ! empty( $user_votes ) ) {
				foreach ( $user_votes as $user_vote ){
					if ( $current_user->ID === $user_vote ) {
						$i = true;
					}
				}
				if ( ! $i ) {
					++$votes;
					update_post_meta( $_POST['post_id'], 'yith_pt_votes_down', $votes );
					array_push( $user_votes, $current_user->ID );
					update_post_meta( $_POST['post_id'], 'yith_pt_user_vote', $user_votes );
				}
				$output = array( 'votes' => $votes );
			} else {
				++$votes;
				update_post_meta( $_POST['post_id'], 'yith_pt_votes_down', $votes );
				$user_votes = array($current_user->ID);
				update_post_meta( $_POST['post_id'], 'yith_pt_user_vote', $user_votes );
				$output = array( 'votes' => $votes );
			}

			echo wp_json_encode( $output );
			die();

		}

	}
}
