<?php
/**
 * Widgets_Exercise Plugin 1
 *
 * @package Shortcode_Excercise
 * @version 1.0.0
 */

/*
Plugin Name: My Widget
Plugin URI: http://wordpress.org/extend/plugins/#
Description: This is an example plugin
Author: German Fernandez
Version: 1.0
Author URI: http://example.test/
*/

/**
 * Yith_Gf_Widget
 */
class Yith_Gf_Widget extends WP_Widget {

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		$widget_ops = array(
			'classname'   => 'yith_gf_widget',
			'description' => 'Mi primer plugin',
		);
		parent::__construct( 'yith_gf_widget', 'Mi primer widget', $widget_ops );
	}


	/**
	 * Widget.
	 *
	 * @param  mixed $args Args.
	 * @param  mixed $instance Instance.
	 * @return void
	 */
	public function widget( $args, $instance ) {
		echo esc_attr( $args['before_widget'] );
		?>
		<div>
			<?php if ( ! empty( $instance['nombre'] ) ) { ?>
				<p><?php echo esc_html( __( 'Nombre: ' ) ) . esc_attr( $instance['nombre'] ); ?></p><br>
			<?php } ?>
			<?php if ( ! empty( $instance['apellido'] ) ) { ?>
				<p><?php echo esc_html( __( 'Apellido: ' ) ) . esc_attr( $instance['apellido'] ); ?></p><br>
			<?php } ?>
			<?php if ( ! empty( $instance['apellido'] ) ) { ?>
				<p><?php echo esc_html( __( 'Dirección: ' ) ) . esc_attr( $instance['direccion'] ); ?></p><br>
			<?php } ?>
			<?php if ( ! empty( $instance['apellido'] ) ) { ?>
				<p><?php echo esc_html( __( 'Telefono: ' ) ) . esc_attr( $instance['telefono'] ); ?></p><br>
			<?php } ?>
		</div>	 
		<?php
		echo esc_attr( $args['after_widget'] );
	}

	/**
	 * Output the option form field in admin Widgets screen.
	 *
	 * @param  mixed $instance Instance.
	 * @return void
	 */
	public function form( $instance ) {
		$nombre    = ! empty( $instance['nombre'] ) ? $instance['nombre'] : '';
		$apellido  = ! empty( $instance['apellido'] ) ? $instance['apellido'] : '';
		$direccion = ! empty( $instance['direccion'] ) ? $instance['direccion'] : '';
		$telefono  = ! empty( $instance['telefono'] ) ? $instance['telefono'] : '';
		?>

		<p>	
			<!-- Nombre -->	

			<label for="<?php echo esc_attr( $this->get_field_id( 'nombre' ) ); ?>">				 
				<?php esc_attr_e( 'Nombre:', 'text_domain' ); ?>			 
			</label>	

			<input					 
				class="widefat"					 
				id="<?php echo esc_attr( $this->get_field_id( 'nombre' ) ); ?>"					 
				name="<?php echo esc_attr( $this->get_field_name( 'nombre' ) ); ?>"					 
				type="text"					 
				value="<?php echo esc_attr( $nombre ); ?>">	

			<!-- Apellido -->	
			<label for="<?php echo esc_attr( $this->get_field_id( 'apellido' ) ); ?>">				 
				<?php esc_attr_e( 'Apellido:', 'text_domain' ); ?>			 
			</label>	

			<input					 
				class="widefat"					 
				id="<?php echo esc_attr( $this->get_field_id( 'apellido' ) ); ?>"					 
				name="<?php echo esc_attr( $this->get_field_name( 'apellido' ) ); ?>"					
				type="text"										 
				value="<?php echo esc_attr( $apellido ); ?>">

			<!-- direccion -->	
			<label for="<?php echo esc_attr( $this->get_field_id( 'direccion' ) ); ?>">				 
				<?php esc_attr_e( 'Direccion:', 'text_domain' ); ?>			 
			</label>	

			<input					 
				class="widefat"					 
				id="<?php echo esc_attr( $this->get_field_id( 'direccion' ) ); ?>"					 
				name="<?php echo esc_attr( $this->get_field_name( 'direccion' ) ); ?>"					 
				type="text"										 
				value="<?php echo esc_attr( $direccion ); ?>">	

			<!-- telefono -->	
			<label for="<?php echo esc_attr( $this->get_field_id( 'telefono' ) ); ?>">				 
				<?php esc_attr_e( 'Telefono:', 'text_domain' ); ?>			 
			</label>

			<input					 
				class="widefat"					 
				id="<?php echo esc_attr( $this->get_field_id( 'telefono' ) ); ?>"					 
				name="<?php echo esc_attr( $this->get_field_name( 'telefono' ) ); ?>"					 
				type="text"										  
				value="<?php echo esc_attr( $telefono ); ?>">	
		</p>
		<?php
	}

	/**
	 * Update
	 *
	 * @param  mixed $new_instance New Instance.
	 * @param  mixed $old_instance Old Instance.
	 * @return $instance
	 */
	public function update( $new_instance, $old_instance ) {
		$instance              = array();
		$instance['nombre']    = ( ! empty( $new_instance['nombre'] ) ) ? wp_strip_all_tags( $new_instance['nombre'] ) : '';
		$instance['apellido']  = ( ! empty( $new_instance['apellido'] ) ) ? wp_strip_all_tags( $new_instance['apellido'] ) : '';
		$instance['direccion'] = ( ! empty( $new_instance['direccion'] ) ) ? wp_strip_all_tags( $new_instance['direccion'] ) : '';
		$instance['telefono']  = ( ! empty( $new_instance['telefono'] ) ) ? wp_strip_all_tags( $new_instance['telefono'] ) : '';
		return $instance;
	}
}

// Register Yith_Gf_Widget.
add_action(
	'widgets_init',
	function() {
		register_widget( 'yith_gf_widget' );
	}
);

