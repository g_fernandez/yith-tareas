<?php
/**
 * Description.
 *
 * @package Widgets_exercise2
 */

/**
 * Plugin Name: Posts Widget
 * Plugin URI: http://wordpress.org/extend/plugins/#
 * Description: This is an example plugin
 * Author: German Fernandez
 * Version: 1.0
 * Author URI: http://example.test/
 */

/**
 * Yith_Gf_Posts_Widget
 */
class Yith_Gf_Posts_Widget extends WP_Widget {
	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		$widget_ops = array(
			'classname'   => 'yith_gf_posts_widget',
			'description' => 'Listar ultimos 5 posts',
		);
		parent::__construct( 'yith_gf_posts_widget', 'Listar ultimos 5 posts', $widget_ops );
	}

	/**
	 * Output the widget content on the front-end
	 *
	 * @param  mixed $args Args.
	 * @param  mixed $instance Instance.
	 * @return void
	 */
	public function widget( $args, $instance ) {
		echo esc_attr( $args['before_widget'] );
		$posts = get_posts();
		?>
		<h2 class="widget-title subheading heading-size-3">Ultimos 5 Posts</h2>
		<nav role="navigation">
			<ul>
				<?php
				foreach ( $posts as $key => $post ) {
					if ( $key <= 4 ) {
						$titulo = $post->post_title;
						?>
						<li>
							<a href="<?php echo esc_url( get_permalink( $post->ID ) ); ?>"><?php echo esc_attr( $titulo ); ?></a>
						</li>
						<?php
					}
				}
				?>
			</ul>
		</nav>
		<?php
		echo esc_attr( $args['after_widget'] );
	}

	/**
	 * Output the option form field in admin Widgets screen.
	 *
	 * @param  mixed $instance Instance.
	 * @return void
	 */
	public function form( $instance ) {
	}

	/**
	 * Update - Save options.
	 *
	 * @param  mixed $new_instance New Instance.
	 * @param  mixed $old_instance Old Instance.
	 * @return void
	 */
	public function update( $new_instance, $old_instance ) {
	}
}

// register yith_gf_posts_widget.
add_action(
	'widgets_init',
	function() {
		register_widget( 'yith_gf_posts_widget' );
	}
);
