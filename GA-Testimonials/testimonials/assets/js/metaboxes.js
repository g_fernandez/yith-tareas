jQuery( document ).ready(function() {
  jQuery('.my-color-field').wpColorPicker();

  if ( jQuery( '#testimonial_meta_badge' ).prop( 'checked' ) ) {
    jQuery( '#badge' ).show();
  }else{
    jQuery( '#badge' ).hide();
  }

  jQuery('#testimonial_meta_badge').change(function() {
    if(this.checked) {
      jQuery( '#badge' ).show();
    }else{
      jQuery( '#badge' ).hide();
    }
  });
});
