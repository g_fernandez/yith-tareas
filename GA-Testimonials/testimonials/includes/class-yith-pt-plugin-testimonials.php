<?php
/**
 * YITH PT Plugin Testimonials.
 *
 * @package testimonials
 */

if ( ! defined( 'YITH_PT_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PT_Plugin_Testimonials' ) ) {
	/**
	 * YITH_PT_Plugin_Testimonials
	 */
	class YITH_PT_Plugin_Testimonials {

		/**
		 * Plugin Testimonials instance.
		 *
		 * @var YITH_PT_Plugin_Testimonials
		 */
		private static $instance;

		/**
		 * Admin class instance.
		 *
		 * @var YITH_PT_Admin
		 */
		public $admin = null;

		/**
		 * Frontend class instance.
		 *
		 * @var YITH_PT_Frontend
		 */
		public $frontend = null;

		/**
		 * Shortcodes class instance.
		 *
		 * @var YITH_PT_Shortcodes
		 */
		public $shortcodes = null;


		/**
		 * Get_instance
		 *
		 * @return YITH_PT_Plugin_Testimonials Main instance.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();

		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

			$require = apply_filters(
				'yith_pt_require_class',
				array(
					'common'   => array(
						'includes/class-yith-pt-post-types.php',
						'includes/class-yith-pt-shortcodes.php',
						'includes/class-yith-pt-transient-widget.php',
						'includes/functions.php',
					),
					'admin'    => array(
						'includes/class-yith-pt-admin.php',
					),
					'frontend' => array(
						'includes/class-yith-pt-frontend.php',
					),
				)
			);

			$this->_require( $require );

			$this->init_classes();

			$this->init();

		}

		/**
		 * _require
		 *
		 * @param mixed $main_classes Main.
		 * @return void
		 */
		protected function _require( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' == $section || ( 'frontend' == $section && ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' == $section && is_admin() ) && file_exists( YITH_PT_DIR_PATH . $class ) ) {
						require_once( YITH_PT_DIR_PATH . $class );
					}
				}
			}
		}

		/**
		 * Init_classes
		 *
		 * @return void
		 */
		public function init_classes() {
			$this->shortcodes = YITH_PT_Shortcodes::get_instance();
			YITH_PT_Post_Types::get_instance();
		}

		/**
		 * Init
		 *
		 * @return void
		 */
		public function init() {

			if ( is_admin() ) {
				$this->admin = YITH_PT_Admin::get_instance();
			}

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = YITH_PT_Frontend::get_instance();
			}
		}

	}

}

if ( ! function_exists( 'yith_pt_plugin_testimonials' ) ) {
	/**
	 * Yith_pt_plugin_testimonials
	 *
	 * @return YITH_PT_Plugin_Testimonials Instance.
	 */
	function yith_pt_plugin_testimonials() {
		return YITH_PT_Plugin_Testimonials::get_instance();
	}
}
